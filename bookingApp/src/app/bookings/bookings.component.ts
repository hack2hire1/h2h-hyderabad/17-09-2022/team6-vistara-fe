import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BookingsService } from '../bookings.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})

export class BookingsComponent implements OnInit {
  form: FormGroup;
  loading = false;
  submitted = false;
  flights: any = [];
  bookingflight: any;

  constructor(
    private formBuilder: FormBuilder,
    private bookingService: BookingsService
  ) {

    this.form = this.formBuilder.group({
      source: [''],
      destination: [''],
      datetime: ['']
    });
  }

  ngOnInit() {
  
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  Search() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    this.flights = [
      { name: "Flight 1", timings: ['10:00 AM', '1:00 PM', '5:00PM'] },
      { name: "Flight 2", timings: ['10:00 AM', '1:00 PM', '5:00PM'] },
      { name: "Flight 3", timings: ['10:00 AM', '1:00 PM', '5:00PM'] }
    ];

    // to call the bookings service
    // this.bookingService.searchFlights(this.form.value).subscribe((res) => {
    //   this.flights = res;
    // });


  }
  public clickFlightBooking(obj: any) {
    this.bookingflight = obj;
  }

  public BookFlight(flight: any, time: any): any {

    console.log("Booking Flight Details", flight, time);
    // calling api to book flight
    /**
     * @author Mahender Chakali
     */
    
    this.bookingService.BookFlight({ flight, time }).subscribe((res) => {
      this.flights = res;
    });

  }
}