import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookingsService {

  constructor(private _http: HttpClient) { }

  // searchflights api will call rest api and will return observable
  public searchFlights(request: any): Observable<any> {
    return this._http.post<any>('https://reqres.in/api/posts', { request: request })
  }

  // to book flight
  public BookFlight(request: any): Observable<any> {
    return this._http.post<any>('https://reqres.in/api/posts', { request: request })
  }

}
